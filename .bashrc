# .bashrc

#get git branch name
git_branch_name_to_PS1() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
git_branch_name() {
    echo "$(git_branch_name_to_PS1)" | sed "s/(\(.*\))/\1/"
}

export PS1="\e[1;31m[\u@\h \A]\e[m \w \e[3;32m\$(git_branch_name_to_PS1) \e[m\n%"

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


# User specific aliases and functions
alias lt="ls -latr"
alias tsmc45lay="tsmc45_dgo_11_25"
alias src="source /home/student/pkowalski/.bashrc"

#current working area
alias cdw="cd /home/student/pkowalski/working"
alias cdp="cd /home/student/pkowalski/working/TSMC45_DGO_1125/PZB"
alias cdvdic="cd /home/student/pkowalski/working/VDIC"
alias cdrf="cd /home/student/pkowalski/working/TSMC45RF"

#vim
alias cim="vim"
alias bim="vim"

#git
alias gits="git status"
alias gitl="git log"
alias gita="git add"
alias gitcm="git commit"
alias gitco="git checkout"
alias gitt="git ls-tree -r \$(git_branch_name)"
